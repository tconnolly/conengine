/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.game;

import com.tcon.conengine.game.states.GameState;
import com.tcon.conengine.engine.app.SimpleGameApplication;

/**
 *
 * @author Tom
 */
public class ConCraft extends SimpleGameApplication {
    
    public static final int SCREEN_WIDTH = 800;
    public static final int SCREEN_HEIGHT = 600;
    public static final String TITLE = "ConCraft";
    
    public static final int FPS = 60;
    
    @Override
    public void initApp() {
        displayManager.setScreenWidth(SCREEN_WIDTH);
        displayManager.setScreenHeight(SCREEN_HEIGHT);
        displayManager.setTitle(TITLE);
        displayManager.setTargetFPS(FPS);
        
        stateManager.setState(new GameState());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ConCraft app = new ConCraft();
        app.start();
    }

    @Override
    public void simpleUpdate(float tpf) {
        
    }
    
}
