/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.game.states;

import com.tcon.conengine.game.input.MainMenuInputHandler;
import com.tcon.conengine.engine.managers.InputManager;
import com.tcon.conengine.engine.states.CCState;

/**
 *
 * @author Tom
 */
public class MainMenuState implements CCState {

    public MainMenuState() {
        InputManager.getInstance().setInputHandler(new MainMenuInputHandler());
    }
    
    @Override
    public void update() {
        System.out.println("Main menu state");
    }
    
    @Override
    public void render() {
        
    }
    
}
