/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.game.input;

import com.tcon.conengine.engine.entities.MoveableEntity2D;
import com.tcon.conengine.engine.input.CCInputHandler;
import org.lwjgl.input.Keyboard;

/**
 *
 * @author Tom
 */
public class GameInputHandler implements CCInputHandler {

    private MoveableEntity2D moveableEntity;
    
    @Override
    public void handleInput() {
        if(Keyboard.isKeyDown(Keyboard.KEY_W)) {
            moveableEntity.move(0, -1);
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_S)) {
            moveableEntity.move(0, 1);
        }
    }
    
    public void setMoveable(MoveableEntity2D moveable) {
        moveableEntity = moveable;
    }
}
