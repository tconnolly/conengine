/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.camera;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.*;

/**
 *
 * @author Tom
 */
public class Camera3D {
    protected float x; // x coord
    protected float y; // y coord
    protected float z; // z coord
    
    protected float xRot; // x rotation
    protected float yRot; // y rotation
    protected float zRot; // z rotation
    
    protected float fov; // Field of view
    protected float aspectRatio; // Aspect ratio
    protected float nearClip; // Near clipping plane
    protected float farClip; // Far clipping plane
    
    public Camera3D() {
        
    }
    
    public Camera3D(float fov, float aspectRatio, float nearClip, float farClip) {
        this.fov = fov;
        this.aspectRatio = aspectRatio;
        this.nearClip = nearClip;
        this.farClip = farClip;
    }
    
    public Camera3D(float x, float y, float z, float fov, float aspectRatio, float nearClip, float farClip) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.fov = fov;
        this.aspectRatio = aspectRatio;
        this.nearClip = nearClip;
        this.farClip = farClip;
    }
    
    public Camera3D(float x, float y, float z, float xRot, float yRot, float zRot, float fov, float aspectRatio, float nearClip, float farClip) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.xRot = xRot;
        this.yRot = yRot;
        this.zRot = zRot;
        this.fov = fov;
        this.aspectRatio = aspectRatio;
        this.nearClip = nearClip;
        this.farClip = farClip;
    }
    
    public void init() {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(fov, aspectRatio, nearClip, farClip);
        glMatrixMode(GL_MODELVIEW);
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_DEPTH_TEST);
    }
    
    public void setView() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();
        // Apply x rotation to x-axis
        glRotatef(xRot, 1, 0, 0);
        // Apply y rotation to y-axis
        glRotatef(yRot, 0, 1, 0);
        // Apply z rotation to z-axis
        glRotatef(zRot, 0, 0, 1);
        // Apply x, y and z position
        glTranslatef(x, y, z);
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public float getxRot() {
        return xRot;
    }

    public void setxRot(float xRot) {
        this.xRot = xRot;
    }

    public float getyRot() {
        return yRot;
    }

    public void setyRot(float yRot) {
        this.yRot = yRot;
    }

    public float getzRot() {
        return zRot;
    }

    public void setzRot(float zRot) {
        this.zRot = zRot;
    }

    public float getFov() {
        return fov;
    }

    public void setFov(float fov) {
        this.fov = fov;
    }

    public float getAspectRatio() {
        return aspectRatio;
    }

    public void setAspectRatio(float aspectRatio) {
        this.aspectRatio = aspectRatio;
    }

    public float getNearClip() {
        return nearClip;
    }

    public void setNearClip(float nearClip) {
        this.nearClip = nearClip;
    }

    public float getFarClip() {
        return farClip;
    }

    public void setFarClip(float farClip) {
        this.farClip = farClip;
    }
}
