/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.entities;

import com.tcon.conengine.engine.util.Time;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author Tom
 */
public class Box2D extends Entity2D implements MoveableEntity2D {
    
    private float[] baseColor;
    
    public Box2D() {
        super();
        baseColor = new float[] { rgb[0], rgb[1], rgb[2] };
    }
    
    public Box2D(int x, int y, int w, int h) {
        super(x, y, w, h);

        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        
        baseColor = new float[] { rgb[0], rgb[1], rgb[2] };
    }
    
    @Override
    public boolean inBounds(int x, int y) {
        if(x >= this.x && x <= this.x + w && y >= this.y && y <= this.y + h) {
            setColor(255, 0, 0);
            
            return true;
        } else {
            setColor(baseColor[0], baseColor[1], baseColor[2]);
        }
        return false;
    }

    @Override
    public void draw() {        
        glColor3f(rgb[0], rgb[1], rgb[2]);
        if(texture != null) {
            texture.bind();
            
            glBegin(GL_QUADS);
                    glTexCoord2f(0, 0);
                glVertex2f(x, y);
                    glTexCoord2f(1, 0);
                glVertex2f(x + w, y);
                    glTexCoord2f(1, 1);
                glVertex2f(x + w, y + h);
                    glTexCoord2f(0, 1);
                glVertex2f(x, y + h);
            glEnd();
        } else {
            glBegin(GL_QUADS);
                glVertex2f(x, y);
                glVertex2f(x + w, y);
                glVertex2f(x + w, y + h);
                glVertex2f(x, y + h);
            glEnd();
        }
    }

    @Override
    public void move(int x, int y) {
        System.out.println("Moving box + " + x + " + " + y);
        this.x += x * Time.deltaTime;
        this.y += y * Time.deltaTime;
    }
    
}
