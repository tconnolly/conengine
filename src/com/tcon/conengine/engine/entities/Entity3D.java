/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.entities;

/**
 *
 * @author Tom
 */
public abstract class Entity3D {
    protected float x; // x coord
    protected float y; // y coord
    protected float z; // z coord
    
    protected float w; // Width
    protected float h; // Height
    protected float d; // Depth
    
    public Entity3D() {
        x = 0;
        y = 0;
        z = 0;
    }
    
    public Entity3D(float x, float y, float z, float w, float h, float d) {
        this.x = x;
        this.y = y;
        this.z = z;
        
        this.w = w;
        this.h = h;
        this.d = d;
    }
    
    public abstract void draw();
}
