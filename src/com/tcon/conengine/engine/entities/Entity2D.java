/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.entities;

import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.opengl.Texture;

/**
 *
 * @author Tom
 */
public abstract class Entity2D {
    protected int x;
    protected int y;
    protected int w;
    protected int h;
    
    protected float[] rgb;
    protected Vector2f[] points;
    
    protected Texture texture;
    
    public Entity2D() {
        x = 0;
        y = 0;
        rgb = new float[] { 255, 255, 255 };
    }
    
    public Entity2D(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        
        rgb = new float[] { 255, 255, 255 };
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
    
    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public int getWidth() {
        return w;
    }
    
    public void setWidth(int w) {
        this.w = w;
    }
    
    public int getHeight() {
        return h;
    }
    
    public void setHeight(int h) {
        this.h = h;
    }
    
    public void setColor(float r, float g, float b) {
        rgb[0] = r;
        rgb[1] = g;
        rgb[2] = b;
    }

    public Texture getTexture() {
        return texture;
    }
    
    public void setTexture(Texture texture) {
        this.texture = texture;
    }
    
    public abstract boolean inBounds(int x, int y);
    public abstract void draw();
}
