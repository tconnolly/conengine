/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.entities;

import com.tcon.conengine.engine.util.Time;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author Tom
 */
public class Box3D extends Entity3D {
    
    public Box3D() {
        super();
    }
    
    public Box3D(float x, float y, float z, float w, float h, float d) {
        super(x, y, z, w, h, d);
    }

    @Override
    public void draw() {
        glColor3f(0, 255, 0);
        
        glRotatef(1, 0, 1, 0);
        
        // Front face
        glBegin(GL_QUADS);
            glVertex3f(x, y, z);
            glVertex3f(x + w, y, z);
            glVertex3f(x + w, y + h, z);
            glVertex3f(x, y + h, z);
        glEnd();
        
        // Right face
        glBegin(GL_QUADS);
            glVertex3f(x + w, y, z);
            glVertex3f(x + w, y, z + d);
            glVertex3f(x + w, y + h, z + d);
            glVertex3f(x + w, y + h, z);
        glEnd();
        
        
    }
    
}
