/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.states;

/**
 *
 * @author Tom
 */
public interface CCState {
    public void update();
    public void render();
}
