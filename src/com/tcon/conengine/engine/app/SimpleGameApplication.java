/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.app;

/**
 *
 * @author Tom
 */
public abstract class SimpleGameApplication extends GameApplication {

    @Override
    public void initialise() {
        super.initialise();
        
        // Call user code
        initApp();
    }
    
    @Override
    public void start() {
        initialise();
        super.start();
    }
    
    public abstract void initApp();
    public abstract void simpleUpdate(float tpf);
}
