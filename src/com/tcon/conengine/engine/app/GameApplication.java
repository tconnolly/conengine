/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.app;

import com.tcon.conengine.engine.camera.Camera3D;
import com.tcon.conengine.engine.managers.AssetManager;
import com.tcon.conengine.engine.managers.DisplayManager;
import com.tcon.conengine.engine.managers.GLManager;
import com.tcon.conengine.engine.managers.InputManager;
import com.tcon.conengine.engine.managers.SceneManager2D;
import com.tcon.conengine.engine.managers.StateManager;
import com.tcon.conengine.engine.scene.Node;
import com.tcon.conengine.engine.util.Time;
import org.lwjgl.input.Keyboard;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.opengl.Texture;

/**
 *
 * @author Tom
 */
public class GameApplication {
    protected StateManager stateManager;
    protected AssetManager assetManager;
    protected DisplayManager displayManager;
    protected GLManager glManager;
    protected InputManager inputManager;
    protected SceneManager2D sceneManager;
    
    protected Camera3D camera;
    
    protected Node rootNode;
    
    private boolean running;
    public static boolean CLOSE_REQUESTED = false;
    
    public GameApplication() {}
    
    protected void initStateManager() {
        stateManager = StateManager.getInstance();
    }
    
    protected void initAssetManager() {
        assetManager = AssetManager.getInstance();
    }
    
    protected void initDisplayManager() {
        displayManager = DisplayManager.getInstance();
    }
    
    protected void initGLManager() {
        glManager = GLManager.getInstance();
    }
    
    protected void initInputManager() {
        inputManager = InputManager.getInstance();
    }
    
    protected void initSceneManager2D() {
        sceneManager = SceneManager2D.getInstance();
    }
    
    protected void initCamera3D() {
        camera = new Camera3D(
                70, 
                displayManager.getScreenWidth() / displayManager.getScreenHeight(),
                1,
                1000);
    }
    
    public void initialise() {
        initStateManager();
        initAssetManager();
        initDisplayManager();
        initGLManager();
        initInputManager();
        initSceneManager2D();
        initCamera3D();
        
        rootNode = new Node("root");
        
        running = false;
    }
    
    public void start() {        
        stateManager.start();
        displayManager.start();
        glManager.start();
        inputManager.start();
        sceneManager.start();
        
        camera.init();
        
        running = true;
        Time.updateDeltaTime();
        float x  = 0;
        
        Texture wood = assetManager.loadTexture(AssetManager.TextureFileType.JPG, "wood");
        
        while(running) {
            displayManager.startLoop();
            
            stateManager.handle();
            
            camera.setView();
            //sceneManager.render();
            
            glPushMatrix();
            {
                glColor3f(1, 0.5f, 0);
                glTranslatef(0, 0, -5);
                glRotatef(x, 1, 1, 0);
                
                if(Keyboard.isKeyDown(Keyboard.KEY_W)) {
                    camera.setZ(camera.getZ() + 0.05f);
                }
                if(Keyboard.isKeyDown(Keyboard.KEY_S)) {
                    camera.setZ(camera.getZ() - 0.05f);
                }
                if(Keyboard.isKeyDown(Keyboard.KEY_A)) {
                    camera.setX(camera.getX() + 0.05f);
                }
                if(Keyboard.isKeyDown(Keyboard.KEY_D)) {
                    camera.setX(camera.getX() - 0.05f);
                }
                
                if(Keyboard.isKeyDown(Keyboard.KEY_UP)) {
                    camera.setxRot(camera.getxRot() - 0.5f);
                }
                if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
                    camera.setxRot(camera.getxRot() + 0.5f);
                }
                if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
                    camera.setyRot(camera.getyRot() - 0.5f);
                }
                if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
                    camera.setyRot(camera.getyRot() + 0.5f);
                }
                
                // front face
                glBegin(GL_QUADS);
                {
                    // front face
                    glColor3f(1, 1, 1);
                    wood.bind();
                    glTexCoord2f(0, 0); glVertex3f(-1, 1, 1);
                    glTexCoord2f(1, 0); glVertex3f(1, 1, 1);
                    glTexCoord2f(1, 1); glVertex3f(1, -1, 1);
                    glTexCoord2f(0, 1); glVertex3f(-1, -1, 1);
                    
                    // back face
                    glColor3f(1, 1, 1);
                    glTexCoord2f(0, 0); glVertex3f(-1, 1, -1);
                    glTexCoord2f(1, 0); glVertex3f(1, 1, -1);
                    glTexCoord2f(1, 1); glVertex3f(1, -1, -1);
                    glTexCoord2f(0, 1); glVertex3f(-1, -1, -1);
                    
                    // bottom face
                    glColor3f(0, 1, 1);
                    glVertex3f(-1, -1, -1);
                    glVertex3f(-1, -1, 1);
                    glVertex3f(1, -1, 1);
                    glVertex3f(1, -1, -1);
                    
                    // top face
                    glColor3f(1, 0, 0);
                    glVertex3f(-1, 1, -1);
                    glVertex3f(-1, 1, 1);
                    glVertex3f(1, 1, 1);
                    glVertex3f(1, 1, -1);
                    
                    // left face
                    glColor3f(1, 1, 0);
                    glVertex3f(-1, 1, -1);
                    glVertex3f(-1, 1, 1);
                    glVertex3f(-1, -1, 1);
                    glVertex3f(-1, -1, -1);
                
                    // right face
                    glColor3f(1, 0, 1);
                    glVertex3f(1, 1, -1);
                    glVertex3f(1, 1, 1);
                    glVertex3f(1, -1, 1);
                    glVertex3f(1, -1, -1);
                }
                glEnd();
            }
            
            
            glPopMatrix();
            
            x += 0.5f;
            
            displayManager.endLoop();
            Time.updateDeltaTime();
            if(CLOSE_REQUESTED) {
                running = false;
            }
        }
    }
    
    public void stop() {
        running = false;
    }
}
