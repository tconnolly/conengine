/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.managers;

import com.tcon.conengine.engine.app.GameApplication;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author Tom
 */
public class DisplayManager implements CCManager {
    private static DisplayManager instance;
    
    private int screenWidth;
    private int screenHeight;
    private String title;
    private int targetFPS;
    
    /**
     * Initialises DisplayManager with default values.
     */
    private DisplayManager() {
        screenWidth = 640;
        screenHeight= 480;
        title = "ConEngine Game";
        targetFPS = 60;
    }
    
    public static DisplayManager getInstance() {
        if(instance == null)
            instance = new DisplayManager();
        return instance;
    }
    
    /**
     * Initialises the LWJGL display.
     */
    @Override
    public void start() {
        try {
            Display.setDisplayMode(new DisplayMode(
                    screenWidth, screenHeight));
            Display.setTitle(title);
            Display.create();
        } catch (LWJGLException ex) {
            Logger.getLogger(DisplayManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void stop() {
        
    }
    
    @Override
    public void handle() {
        
    }
    
    public void startLoop() {
        
    }
    
    public void endLoop() {
        Display.update();
        Display.sync(targetFPS);
        
        if(Display.isCloseRequested()) {
            GameApplication.CLOSE_REQUESTED = true;
        }
    }
    
    public int getScreenWidth() {
        return screenWidth;
    }
    
    public void setScreenWidth(int screenWidth) {
        this.screenWidth = screenWidth;
    }
    
    public int getScreenHeight() {
        return screenHeight;
    }
    
    public void setScreenHeight(int screenHeight) {
        this.screenHeight = screenHeight;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public int getTargetFPS() {
        return targetFPS;
    }
    
    public void setTargetFPS(int targetFPS) {
        this.targetFPS = targetFPS;
    }
}
