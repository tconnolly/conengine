/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.managers;

import com.tcon.conengine.engine.entities.Entity3D;
import java.util.ArrayList;

/**
 *
 * @author Tom
 */
public class SceneManager3D implements CCManager {
    private static SceneManager3D instance;
    
    private ArrayList<Entity3D> entities;
    
    private SceneManager3D() {
        entities = new ArrayList<>();
    }
    
    public static SceneManager3D getInstance() {
        if(instance == null)
            instance = new SceneManager3D();
        return instance;
    }

    @Override
    public void start() {
        
    }

    @Override
    public void stop() {
        
    }
    
    @Override
    public void handle() {
        render();
    }
    
    public void render() {
        for(int i = 0; i < entities.size(); i++) {
            entities.get(i).draw();
        }
    }
    
    public void addEntity(Entity3D entity) {
        entities.add(entity);
    }
    
    public void removeEntity(Entity3D entity) {
        entities.remove(entity);
    }
    
    public void clearEntities() {
        entities.clear();
    }
}