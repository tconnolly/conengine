/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.managers;

/**
 *
 * @author Tom
 */
public interface CCManager {
    public void start();
    public void stop();
    public void handle();
}
