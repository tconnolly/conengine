/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.managers;

import com.tcon.conengine.engine.entities.Entity2D;
import java.util.ArrayList;

/**
 *
 * @author Tom
 */
public class SceneManager2D implements CCManager {
    private static SceneManager2D instance;
    
    private ArrayList<Entity2D> entities;
    
    private SceneManager2D() {
        entities = new ArrayList<>();
    }
    
    public static SceneManager2D getInstance() {
        if(instance == null)
            instance = new SceneManager2D();
        return instance;
    }

    @Override
    public void start() {
        
    }

    @Override
    public void stop() {
        
    }
    
    @Override
    public void handle() {
        render();
    }
    
    public void render() {
        for(int i = 0; i < entities.size(); i++) {
            entities.get(i).draw();
        }
    }
    
    public void addEntity(Entity2D entity) {
        entities.add(entity);
    }
    
    public void removeEntity(Entity2D entity) {
        entities.remove(entity);
    }
    
    public void clearEntities() {
        entities.clear();
    }
}
