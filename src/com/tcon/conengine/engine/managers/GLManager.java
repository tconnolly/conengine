/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.managers;

import static org.lwjgl.opengl.GL11.*;

/**
 *
 * @author Tom
 */
public class GLManager implements CCManager {
    private static GLManager instance;
    
    private GLManager() {}
    
    public static GLManager getInstance() {
        if(instance == null)
            instance = new GLManager();
        return instance;
    }
    
    /**
     * OpenGL initialisation
     */
    @Override
    public void start() {

    }

    @Override
    public void stop() {
        
    }
    
    @Override
    public void handle() {
        
    }
    
}
