/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.managers;

import com.tcon.conengine.engine.states.CCState;
import com.tcon.conengine.engine.states.DefaultState;

/**
 *
 * @author Tom
 */
public class StateManager implements CCManager {
    private static StateManager instance;
    private CCState currentState;
    
    private StateManager() {
        currentState = new DefaultState();
    }
    
    public static StateManager getInstance() {
        if(instance == null)
            instance = new StateManager();
        return instance;
    }

    @Override
    public void start() {
        
    }

    @Override
    public void stop() {
        
    }
    
    @Override
    public void handle() {
        currentState.update();
        currentState.render();
    }
    
    public void setState(CCState newState) {
        currentState = newState;
    }
    
}
