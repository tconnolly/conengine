/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.managers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

/**
 *
 * @author Tom
 */
public class AssetManager implements CCManager {
    private static AssetManager instance;
    
    public enum TextureFileType {
        JPG("jpg"),
        PNG("png");
        
        private final String fileType;
        
        private TextureFileType(String fileType) {
            this.fileType = fileType;
        }
        
        public String getFileType() {
            return fileType;
        }
    }
    
    private AssetManager() {}
    
    public static AssetManager getInstance() {
        if(instance == null)
            instance = new AssetManager();
        return instance;
    }
    
    public Texture loadTexture(TextureFileType fileType, String fileName) {
        try {
            return TextureLoader.getTexture(fileType.getFileType(),
                    new FileInputStream("res/" + fileName + "." + fileType.getFileType()));            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AssetManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AssetManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void start() {
        
    }

    @Override
    public void stop() {
        
    }
    
    @Override
    public void handle() {
        
    }
    
}
