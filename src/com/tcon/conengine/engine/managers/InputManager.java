/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.managers;

import com.tcon.conengine.engine.input.CCInputHandler;

/**
 *
 * @author Tom
 */
public class InputManager implements CCManager {
    private static InputManager instance;
    private CCInputHandler currentInputHandler;
    
    private InputManager() {}
    
    public static InputManager getInstance() {
        if(instance == null)
            instance = new InputManager();
        return instance;
    }

    @Override
    public void start() {
        
    }

    @Override
    public void stop() {
        
    }
    
    @Override
    public void handle() {
        currentInputHandler.handleInput();
    }
    
    public CCInputHandler getInputHandler() {
        return currentInputHandler;
    }
    
    public void setInputHandler(CCInputHandler newInputHandler) {
        currentInputHandler = newInputHandler;
    }
}
