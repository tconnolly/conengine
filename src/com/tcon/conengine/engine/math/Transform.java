/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.math;

import org.lwjgl.util.vector.Quaternion;

/**
 *
 * @author Tom
 */
public class Transform {
    private Quaternion rot = new Quaternion();
    private Vector3f translation;
    private Vector3f scale = new Vector3f(1, 1, 1);
    
    public Transform(Vector3f translation, Quaternion rot) {
        this.translation.set(translation);
        this.rot.set(rot);
    }
    
    public Transform(Quaternion rot) {
        this(Vector3f.ZERO, rot);
    }
}
