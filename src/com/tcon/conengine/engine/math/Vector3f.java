/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.math;

/**
 *
 * @author Tom
 */
public class Vector3f {
    public static final Vector3f ZERO = new Vector3f(0, 0, 0);
    
    public float x;
    public float y;
    public float z;
    
    public Vector3f() {
        x = y = z = 0;
    }
    
    public Vector3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public Vector3f set(Vector3f vec) {
        this.x = vec.x;
        this.y = vec.y;
        this.z = vec.z;
        
        return this;
    }
    
    public Vector3f add(Vector3f vec) {
        return new Vector3f(x + vec.x, y + vec.y, z + vec.z);
    }
    
    public Vector3f subtract(Vector3f vec) {
        return new Vector3f(x - vec.x, y - vec.y, z - vec.z);
    }
    
    public Vector3f scale(float scale) {
        return new Vector3f(x * scale, y * scale, z * scale);
    }
}
