/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.scene;

import java.util.ArrayList;

/**
 *
 * @author Tom
 */
public class Node extends Spatial {
    protected ArrayList<Spatial> children = new ArrayList<>();

    public Node() {}
    
    public Node(String name) {
        super(name);
    }
    
    public int getChildrenSize() {
        return children.size();
    }
    
    public int attachChild(Spatial child) {
        if(child.getParent() != this && child != this) {
            child.setParent(this);
            children.add(child);
        }
        
        return children.size();
    }
    
    public int detachChild(Spatial child) {
        child.setParent(null);
        children.remove(child);
        return children.size();
    }
}
