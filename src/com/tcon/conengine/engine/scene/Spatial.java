/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.scene;

/**
 *
 * @author Tom
 */
public abstract class Spatial {
    protected String name;
    protected Node parent;
    
    public Spatial() {}
    
    public Spatial(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Node getParent() {
        return parent;
    }
    
    protected void setParent(Node parent) {
        this.parent = parent;
    }
}
