/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tcon.conengine.engine.util;

import org.lwjgl.Sys;

/**
 *
 * @author Tom
 */
public class Time {
    private static long lastFrame = 0;
    public static int deltaTime = 0;
    
    public static void updateDeltaTime() {
        long time = getTime();
        deltaTime = (int) (time - lastFrame);
        lastFrame = time;
    }
    
    public static long getTime() {
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }
}
